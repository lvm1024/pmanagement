﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PManagement.Common
{
    public class CommonType
    {
        public enum StatueType
        {
            None,
            //成功
            Sucess = 200,
            //已完成
            Completed = 300,
            //失败
            Fail = 400
        }

        public enum LoginType
        {
            /// <summary>
            /// 登录成功
            /// </summary>
            LoginSucess = 201,
            /// <summary>
            /// 注册成功
            /// </summary>
            RigisterSucess = 202,

            /// <summary>
            /// 已注册
            /// </summary>
            RigisterCompleted =  302,

            /// <summary>
            /// 登录失败
            /// </summary>
            LoginFail = 401,
            /// <summary>
            /// 注册失败
            /// </summary>
            RigisterFail = 402,
        }

        public enum InsertInfo
        {
            AllInfoSucess = 211,
            AllInfoFail = 411
        }

        public enum AbnormalType
        {
            /// <summary>
            /// 异常
            /// </summary>
            Abnormal = 1000,
            /// <summary>
            /// 查询type异常
            /// </summary>
            QueryCategoryAbnormal = 1001,
            /// <summary>
            /// 查询name,remarks异常
            /// </summary>
            QueryInfoAbnormal = 1002,
            /// <summary>
            /// 查询name,remarks异常
            /// </summary>
            QueryAllInfoAbnormal = 1003
        }





        public static string SUCESS = "Sucess";
        public static string FAIL = "Fail";
    }


   
}