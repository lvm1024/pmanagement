﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http.Results;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using static PManagement.Common.CommonType;

namespace PManagement.Common
{
    public class CommonMethod
    {
        /// <summary>
        /// 获取JsonResult里的属性值，以字符串形式返回
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="name"></param>
        /// <param name="json"></param>
        /// <returns></returns>
        public static int GetJsonValue(string name, JsonResult json) 
        {
            
            //先序列化为json字符串
            JavaScriptSerializer jss = new JavaScriptSerializer();
            string myJson = jss.Serialize(json.Data);
            //json字符串在获取属性值
            var jObject = JObject.Parse(myJson);
            int temp = (int)jObject[name];
            return temp;
        }

        /// <summary>
        ///  字符串转枚举
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="name"></param>
        /// <returns></returns>
        public static T GetTargetEnum<T>(string name)
        {
            if (!string.IsNullOrEmpty(name))
            {
                T type = (T)System.Enum.Parse(typeof(T), name);
                return type;
            }
            else
            {
                return default;   
            }
          
        }

        /// <summary>
        /// 设置JsonResult的值，用作返回值
        /// </summary>
        /// <param name="code"></param>
        /// <param name="status"></param>
        /// <param name="loginType"></param>
        /// <returns></returns>
        public static JsonResult SetJsonResult<T>(string code, StatueType status, T loginType)
        {
            JsonResult temp = new JsonResult()
            {
                Data = new
                {
                    Code = code,
                    Status = status,
                    DataResult = new
                    {
                        isResult = loginType
                    }
                },
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
            return temp;
        }

    }
}