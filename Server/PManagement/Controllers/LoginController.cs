﻿using MySql.Data.MySqlClient;
using PManagement.Common;
using PManagement.Models;
using System;
using System.Web.Mvc;
using static PManagement.Common.CommonType;

namespace PManagement.Controllers
{
    public class LoginController : Controller
    {
        private LoginUser loginUser = new LoginUser();
        private MySqlConnection mySql = DBHandle.DBHandle.GetInstance();

        // GET: Login
        public ActionResult Index()
        {
            return View();
        }
        #region 外部API
        /// <summary>
        /// 查询登录
        /// 访问地址：/Login/QueryLogin?username=123&password=123
        /// </summary>
        /// <returns></returns>
        public JsonResult QueryLogin(string username, string password)
        {
            string sql = "SELECT * FROM login WHERE username = @username and password = @password;";
            MySqlCommand cmd = new MySqlCommand(sql, mySql);
            cmd.Parameters.AddWithValue("@username", username);
            cmd.Parameters.AddWithValue("@password", password);
            MySqlDataReader result = cmd.ExecuteReader();//执行查询，并返回查询结果集中第一行的第一列。所有其他的列和行将被忽略。select语句无记录返回时，ExecuteScalar()返回NULL值
            if (result.Read())
            {
                mySql.Close();
                return CommonMethod.SetJsonResult(SUCESS, StatueType.Sucess, LoginType.LoginSucess);
            }
            else
            {
                mySql.Close();
                return CommonMethod.SetJsonResult(SUCESS, StatueType.Fail, LoginType.LoginFail);
            }
        }

        /// <summary>
        /// 注册用户
        /// 访问地址：/Login/RigisterUser?username=123&password=123
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public JsonResult RigisterUser(string username, string password)
        {
            JsonResult queryResult = QueryLoginForRigister(username, password);
            //先序列化为json字符串
            int status = CommonMethod.GetJsonValue("Status", queryResult);
            if ((StatueType)status == StatueType.Sucess)
            {
                mySql.Close();
                return CommonMethod.SetJsonResult(SUCESS, StatueType.Sucess, LoginType.RigisterCompleted);
            }
            else
            {
                try
                {
                    string sql = "INSERT INTO login(username, password) VALUES(@username, @password);";
                    MySqlCommand cmd = new MySqlCommand(sql, mySql);
                    cmd.Parameters.AddWithValue("@username", username);
                    cmd.Parameters.AddWithValue("@password", password);
                    int result = cmd.ExecuteNonQuery();
                    mySql.Close();
                    return CommonMethod.SetJsonResult(SUCESS, StatueType.Sucess, LoginType.RigisterSucess);
                }
                catch (Exception e)
                {

                    mySql.Close();
                    return CommonMethod.SetJsonResult(FAIL, StatueType.Fail, AbnormalType.Abnormal);

                    throw e;
                }

            }

        }
        #endregion


        #region 内部使用
        /// <summary>
        /// 注册前查询有无当前用户
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public JsonResult QueryLoginForRigister(string username, string password)
        {
            string sql = "SELECT * FROM login WHERE username = @username and password = @password;";
            MySqlCommand cmd = new MySqlCommand(sql, mySql);
            cmd.Parameters.AddWithValue("@username", username);
            cmd.Parameters.AddWithValue("@password", password);
            MySqlDataReader result = cmd.ExecuteReader();//执行查询，并返回查询结果集中第一行的第一列。所有其他的列和行将被忽略。select语句无记录返回时，ExecuteScalar()返回NULL值
            if (result.Read())
            {
                result.Close();
                return CommonMethod.SetJsonResult(SUCESS, StatueType.Sucess, LoginType.LoginSucess);
            }
            else
            {
                result.Close();
                return CommonMethod.SetJsonResult(SUCESS, StatueType.Fail, LoginType.LoginFail);
            }
                
        }
        #endregion

        public JsonResult InquiryInformation(string username, string password)
        {
            string sql = "SELECT * FROM login WHERE username = @username and password = @password;";
            MySqlCommand cmd = new MySqlCommand(sql, mySql);
            cmd.Parameters.AddWithValue("@username", username);
            cmd.Parameters.AddWithValue("@password", password);
            MySqlDataReader result = cmd.ExecuteReader();//执行查询，并返回查询结果集中第一行的第一列。所有其他的列和行将被忽略。select语句无记录返回时，ExecuteScalar()返回NULL值
            if (result.Read())
            {
                JsonResult temp = new JsonResult()
                {
                    Data = new
                    {
                        Code = SUCESS,
                        Status = StatueType.Sucess,
                        DataResult = new
                        {
                            username = result.GetString("username"),
                            password = result.GetString("password")
                        }
                    },
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
                mySql.Close();
                return temp;
            }
            else
            {
                JsonResult temp = new JsonResult()
                {
                    Data = new
                    {
                        Code = SUCESS,
                        Status = StatueType.Fail,
                        DataResult = new { }
                    },
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
                mySql.Close();
                return temp;
            }
        }
    }
}