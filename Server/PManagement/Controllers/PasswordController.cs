﻿using MySql.Data.MySqlClient;
using PManagement.Common;
using PManagement.Models;
using System.Collections.Generic;
using System.Web.Mvc;
using static PManagement.Common.CommonType;

namespace PManagement.Controllers
{
    public class PasswordController : Controller
    {
        private Password pwd = new Password();
        private MySqlConnection mySql = DBHandle.DBHandle.GetInstance();
        //API:Products/GetAll
        public JsonResult GetAll()
        {
            return new JsonResult()
            {
                Data = pwd,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        /// <summary>
        /// 查询账号密码信息
        /// 路径：/Password/QueryInfo?type=QQ
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public JsonResult QueryInfo(string type)
        {
            string sql = "SELECT id,name,remarks FROM main_pwd WHERE type = @type ;";
            MySqlCommand cmd = new MySqlCommand(sql, mySql);
            cmd.Parameters.AddWithValue("@type", type);
            try
            {
                MySqlDataReader result = cmd.ExecuteReader();
                List<object> tempData = new List<object>();
                while (result.Read())
                {
                    tempData.Add(new { id = result.GetString("id"), name = result.GetString("name"), remarks = result.GetString("remarks") });
                }
                JsonResult data = new JsonResult()
                {
                    Data = new
                    {
                        Code = SUCESS,
                        Status = StatueType.Sucess,
                        DataResult = tempData
                    },
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
                mySql.Close();
                return data;
            }
            catch (System.Exception e)
            {
                mySql.Close();
                return CommonMethod.SetJsonResult(FAIL, StatueType.Fail, AbnormalType.QueryInfoAbnormal);
            }
        }

        /// <summary>
        /// 查询账号的类别
        /// 路径：/Password/QueryCategory?start=0&&end=1
        /// </summary>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <returns></returns>
        public JsonResult QueryCategory(int start, int end)
        {
            int num = end - start;
            int tempCount = 0;
            string sql = "SELECT DISTINCT type FROM main_pwd  LIMIT  @start,@end;";
            MySqlCommand cmd = new MySqlCommand(sql, mySql);
            cmd.Parameters.AddWithValue("@start", start);
            cmd.Parameters.AddWithValue("@end", end);
            try
            {
                MySqlDataReader result = cmd.ExecuteReader();
                List<object> tempData = new List<object>();
                while (result.Read())
                {
                    tempData.Add(new { type = result.GetString("type") });
                    tempCount++;
                }
                JsonResult data = new JsonResult()
                {
                    Data = new
                    {
                        Code = SUCESS,
                        Status = StatueType.Sucess,
                        DataResult = tempData
                    },
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
                mySql.Close();
                return data;
            }
            catch (System.Exception)
            {
                mySql.Close();
                return CommonMethod.SetJsonResult(FAIL, StatueType.Fail, AbnormalType.QueryCategoryAbnormal);
            }
        }

        /// <summary>
        /// 根据ID查询所有信息
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public JsonResult QueryAll(int id)
        {
            string sql = "SELECT * FROM main_pwd WHERE id = @id ;";
            MySqlCommand cmd = new MySqlCommand(sql, mySql);
            cmd.Parameters.AddWithValue("@id", id);
            try
            {
                MySqlDataReader result = cmd.ExecuteReader();
                List<object> tempData = new List<object>();
                while (result.Read())
                {
                    tempData.Add(new { 
                        type = result.GetString("type"),
                        name = result.GetString("name"), 
                        username = result.GetString("username"),
                        password = result.GetString("password"), 
                        remarks = result.GetString("remarks") 
                    });
                }
                JsonResult data = new JsonResult()
                {
                    Data = new
                    {
                        Code = SUCESS,
                        Status = StatueType.Sucess,
                        DataResult = tempData
                    },
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
                mySql.Close();
                return data;
            }
            catch (System.Exception e)
            {
                mySql.Close();
                return CommonMethod.SetJsonResult(FAIL, StatueType.Fail, AbnormalType.QueryAllInfoAbnormal);
            }
        }

        public JsonResult InsertAll(string type,string name,string username,string password,string remarks)
        {
            string sql = "INSERT INTO main_pwd (type,name,username,password,remarks) VALUES (@type,@name,@username,@password,@remarks)";
            MySqlCommand cmd = new MySqlCommand(sql,mySql);
            cmd.Parameters.AddWithValue("@type",type);
            cmd.Parameters.AddWithValue("@name", name);
            cmd.Parameters.AddWithValue("@username", username);
            cmd.Parameters.AddWithValue("@password", password);
            cmd.Parameters.AddWithValue("@remarks", remarks);
            try
            {
                int row = cmd.ExecuteNonQuery();
                mySql.Close();
                return CommonMethod.SetJsonResult(SUCESS, StatueType.Sucess, InsertInfo.AllInfoSucess);
            }
            catch (System.Exception)
            {
                mySql.Close();
                return CommonMethod.SetJsonResult(FAIL, StatueType.Fail, InsertInfo.AllInfoFail);

            }
            
        }
    }
}
