﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PManagement.Models
{
    public class Password
    {
        /// <summary>
        /// 唯一标识ID
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// 密码类型
        /// </summary>
        public string Type { get; set; }
        /// <summary>
        /// 名字
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 账号
        /// </summary>
        public string Username { get; set; }
        /// <summary>
        /// 密码
        /// </summary>
        public string Pword { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        public string Remarks { get; set; }

    }
}