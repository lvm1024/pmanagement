﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PManagement.Models
{
    public class LoginUser
    {
        /// <summary>
        /// ID
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// 用户名
        /// </summary>
        public string Account { get; set; }
        /// <summary>
        /// 密码
        /// </summary>
        public string Passord { get; set; }
    }
}