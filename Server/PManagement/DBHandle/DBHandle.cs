﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PManagement.DBHandle
{
    public class DBHandle
    {
        public static MySqlConnection GetInstance()
        {
            /*
             * 【data source】=服务器IP地址;
            【database】=数据库名称;
            【user id】=数据库用户名;
            【password】=数据库密码;
            【pooling】=是否放入连接池;
            【charset】=编码方式;
            */

            string connetStr = "data source=112.124.23.64;database=mydata;user id=root;password=123456;pooling=true;charset=utf8;";
            MySqlConnection conn = new MySqlConnection(connetStr);
            //定义SQL Server连接对象
            try
            {
                conn.Open();//打开通道，建立连接，可能出现异常,使用try catch语句
                Console.WriteLine("已经建立连接");
                return conn;
            }
            catch (MySqlException ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
        }
    }
}