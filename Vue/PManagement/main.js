import Vue from 'vue'
import App from './App'

Vue.config.productionTip = false
Vue.prototype.websiteUrl = 'http://112.124.23.64:80'; 


App.mpType = 'app'

const app = new Vue({
	...App
})
app.$mount()
   
   